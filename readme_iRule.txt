Installation and configuration of Notepad++ as iRule editor.
============================================================

Introduction
------------
	Recently I started new job and fortunately had opportunity to work with F5 BIG-IP. 
	Almost from day one I was involved in world of iRules and unfortunately realized that
iRule editor is not exactly what I expected, so as Notepad++ fan I decide to try to make
it works.
	My first challenge was to make solution which use existing tools/plugins with no 
modifications... almost achieved this.

Documentation
------------------
	1. Requirements:
	During my iRule development I found some mandatory features which I can not miss. 
		- Syntax highlighting - only for available TCL commands and iRule syntax.
		- Help - can easily go to proper online help page
		- Autocomplete - again to work with TCL and iRule specific functions and etc
		- plus all other feature which I have in Notepad++ :) - compare, explorer, NppFTP ...
		
	Some of features which I marked as optional and currently are not implemented: 
		- connect to F5 and upload iRule
		- make proper code beautifier
		- syntax check
		
	2. Mandatory components
	To achieve previous requirements following plugins and tools need to be installed:
		- Notepad++ -> "Languages Help" plugin (from plugin manager)
		- Firefox/Chrome/Opera and Redirector extension http://einaregilsson.com/redirector/

	3. Configuration of all tools.
		3.1. Notepad++
			- Configure syntax highlighting:
				Language -> Define your language... -> Import -> Chose the file userDefineLang_iRule.xml
				
			- Copy auto-complete file to proper location:
				copy iRule.xml to .......\Notepad++\plugins\APIs\ folder
				
			- Configure "Language Help" plugin (order is important otherwise ctrl-F1 is not working as expected):
				Plugins -> Language Help -> Options ->
					Add ->
						Name: TCL ->
						Extension: *.tcl -> 
						Extended help file path: http://www.tcl.tk/man/tcl8.4/TclCmd/$word$.htm ->
						Enable showing this entry in Notepad++ help menu: Check
					Add ->
						Name: iRule ->
						Extension: *.tcl -> 
						Extended help file path: https://devcentral.f5.com/wiki/iRules.$word$.ashx ->
						Enable showing this entry in Notepad++ help menu: Check
				
		3.2. Redirector Web Plugin:
			- Configure Firefox/Chrome/Opera extension:
				Redirector -> Edit redirects -> Import -> iRule-Help-Redirector.json
				
			or just use any redirect plugin with following configuration:
				
				Pattern: https://devcentral.f5.com/wiki/iRules.(.*)::(.*).ashx
				Redirect URL: https://devcentral.f5.com/wiki/iRules.$1__$2.ashx

	4. Usage
		4.1. Notepad++
			- File extension of iRule must be .tcl
			- Syntax Highlight should work out of the box for .tcl extensions, if do not work 
				open iRule file and activate it Language -> iRule
			- Help is working:
				- step on/mark whole command -> ctrl-F1
				- step on/mark whole command -> right click -> Language Help -> Chose between TCL or iRule help pages
				Know behaviour is that plugin do not recognize "::" as part of command,
				so you should mark it all for example -> HTTP::enable 